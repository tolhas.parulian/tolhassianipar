from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect

# Create your views here.

def signup_view(request):
	if request.method=="POST":
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user=form.save()
			#log the user in
			login(request, user)
			return redirect("homepage:homepage_index")
	else:
		form = UserCreationForm()
	
	if request.method == "GET":
		if request.user.is_authenticated:
            # logic user
			return redirect ('homepage:homepage_index')
		else:
			return render(request, "accounts/signup.html", {"form": form})
	return render(request, "accounts/signup.html", {"form": form})
	


def login_view(request):
	if request.method=="POST":
		form=AuthenticationForm(data=request.POST)
		if form.is_valid():
			#log in the user
			user=form.get_user()
			login(request, user)
			return redirect("homepage:homepage_index")
	else:
		form=AuthenticationForm()
	return render(request, "accounts/login.html", {"form": form})

# @csrf_protect
@login_required
def logout_view(request):
	if request.method=="POST":
		logout(request)
		return redirect("homepage:homepage_index")
	return redirect("homepage:homepage_index")


        