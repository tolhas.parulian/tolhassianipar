from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.homepage_index, name = 'homepage_index'),
    path('about-me/', views.about_me, name = 'about_me')
]