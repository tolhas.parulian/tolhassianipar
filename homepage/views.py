from django.shortcuts import render

def homepage_index(request):
    return render(request, 'homepage/index.html')

def about_me(request):
    return render(request, 'homepage/about_me.html')
