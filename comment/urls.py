from . import views
from django.urls import path

app_name = 'comment'

urlpatterns = [
    path('', views.comment_list, name='comment_list'),
    path('comment_create/', views.comment_create, name="comment_create"),
    path('reply_create/<int:comment_id>/', views.reply_create, name="reply_create"),
    path("like/", views.like, name="like")
]