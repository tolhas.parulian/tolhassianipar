from . import models
from django import forms


class CreateCommentForm(forms.ModelForm):
    comment = forms.CharField(widget=forms.Textarea(attrs={
            "class" : "md-textarea form-control",
            "required" : True,
            "rows":"3"
             }))
    class Meta:
           model = models.Comment
           fields=[
           "comment"]
           

class CreateReplyForm(forms.ModelForm):
    replies = forms.CharField(widget=forms.Textarea(attrs={
            "class" : "fields",
            "required" : True,
            "placeholder":"Balas komentar dia ^^",
             }))
    class Meta:
        model = models.Reply
        fields =[
        "replies",]