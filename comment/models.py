from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, )
    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.author, self.snipped_comment())

    def snipped_comment(self):
        if len(self.comment) > 20 :
            return self.comment[:20] + " ..."
        return self.comment

class Reply(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,)
    reply_live = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, blank=True, null=True, related_name="replies")

    def __str__(self):
    	return "{} - {}".format(self.author, self.reply_live)

class Like(models.Model):
    username=models.ForeignKey(User, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.username)