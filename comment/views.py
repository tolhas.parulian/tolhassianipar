from django.shortcuts import render, redirect
from .models import Comment, Reply, Like
from .import forms
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


# Create your views here.


def comment_list(request):
    comments = Comment.objects.all()
    comment_form = forms.CreateCommentForm()
    reply_form = forms.CreateReplyForm()
    response = {
        'cmnt' : comments,
        'total_cmnt':len(comments),
        'form' : comment_form,
        'reply_form' : reply_form,
        'likes' : len(Like.objects.all()),
        'first3likes' : Like.objects.all()[:3],
        "restlike" : len(Like.objects.all())-3,

    }
    return render(request, 'comment/comment.html', response)

@login_required(login_url="accounts:login_view")
def comment_create(request):
    comment_form = forms.CreateCommentForm(request.POST)
    if request.method == "POST" and comment_form.is_valid():
        instance=comment_form.save(commit=False)
        instance.author = request.user
        instance.save()
        # Message.objects.create(
        #     message = message_form.cleaned_data['message'],
        # )
    return redirect('comment:comment_list')

@login_required(login_url="accounts:login_view")
def reply_create(request, comment_id):
    reply_form = forms.CreateReplyForm(request.POST)
    comment=Comment.objects.get(pk=comment_id)
    # Comment.objects.get(pk=comment_id).author
    if request.method == "POST" and reply_form.is_valid():
            instance=reply_form.save(commit=False)
            instance.author =request.user
            
            instance.reply_live = reply_form.cleaned_data["replies"]
            instance.comment=comment
            instance.save()
    return redirect('comment:comment_list')

@login_required(login_url="accounts:login_view")
def like(request):
    likers=request.user
    for lk in Like.objects.all():
        if lk.username == likers:
            return redirect("comment:comment_list")
    Like.objects.create(username = request.user)
    return redirect("comment:comment_list")