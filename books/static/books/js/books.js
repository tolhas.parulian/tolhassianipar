var search_field = $("#search_field");
$(document).ready(function(){
    if (getCookie('search-book').length > 0){
        let searchValue = getCookie('search-book');
        bookSearch(searchValue);
        search_field.val(searchValue);
    } else {
        bookSearch('Joyful');
    }
});
bookSearch = (search_value) => {
    $("#list_books").empty();
    if (search_value.length > 0){
        $.ajax({
            type: "GET",
            url: `https://www.googleapis.com/books/v1/volumes?q=${search_value}`,
            dataType: "json",
            success: function (data) {
                for (item of data.items){
                    let authors = item.volumeInfo.authors;
                    if (!authors){
                        authors = ""
                    } else if (authors.length > 1){
                        authors = item.volumeInfo.authors.join(",").replace(",", ", ");
                    }
                    let description = item.volumeInfo.description ? item.volumeInfo.description.substr(0, 30) + "..." : "";
                    let title = item.volumeInfo.title ? item.volumeInfo.title : "";
                    let img = item.volumeInfo.imageLinks.smallThumbnail;
                    if (!img){
                        img = item.volumeInfo.imageLinks.thumbnail;
                    } else if (!img) {
                        img = "https://www.freeiconspng.com/uploads/no-image-icon-8.png"
                    }
                    let previewLink = item.volumeInfo.previewLink ? item.volumeInfo.previewLink : "";
                    let published = item.volumeInfo.publishedDate ? item.volumeInfo.publishedDate : "-";

                    $("#list_books").append(`
                        <div class="mb-5 col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="card">
                                <!-- Card image -->
                                <div class="view view-cascade overlay">
                                    <img  class="card-img-top" src="${img}" alt=${title}">
                                    <a href="${previewLink}">
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                                
                                <!-- Card content -->
                                <div class="card-body card-body-cascade text-center pb-0">
                                
                                    <!-- Title -->
                                    <a href="${previewLink}" class = "books_title">
                                        <h4 class="card-title"><strong>${title}</strong></h4>
                                    </a
                                    <!-- Subtitle -->
                                    <h5 class="blue-text pb-2"><strong>${authors}</strong></h5>
                                    <!-- Text -->
                                    <p class="card-text">${description}</p>
                                
                                    <!-- Card footer -->
                                    <div class="card-footer text-muted text-center mt-4">
                                        Published on: ${published}
                                    </div>
                                </div>
                            </div>
                        </div>`
                    );
                }
            }
        });
        document.cookie = `search-book=${search_value}`;
    }
}

$("#search_button").on('click', function () {
    bookSearch(search_field.val());
});

$( "#search_field" ).keypress(function( event ) {
  if ( event.which == 13 ) {
     event.preventDefault();
     bookSearch(search_field.val());
  }
});

