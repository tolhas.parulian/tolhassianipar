# "Oprec Ristek WebDev"

Web app ini merupakan proyek Portofolio Tolhas untuk Ristek WebDev, yang menggunakan django sebagai library dan juga menggunakan HTML, CSS, dan JS. 

## Daftar isi

Pada proyek ini terdapat:
* Halaman Homepage
* Halaman About Me
* Halaman Contact Me
* Terdapat juga pencarian buku


### Yang digunakan

* Python
* HTML
* CSS
* Bootstrap
* JS (termasuk Cookie)
* Heroku
* Gitlab

### Yang digunakan

Cara menjalankan seperti proyek django normal, dengan python manage.py runserver.
(Info menggunakan django = 2.2.6)

## Dibuat oleh

**Tolhas Parulian Jonathan** dengan berbagai inspirasi GSGS dan bantuan teman-teman.

## Acknowledgments

* The Net Ninja
* Teman-teman
* YouTube
* Google
* Lingkungan sekitar
* Kearifan budaya lokal
* Dan lain sebagainya