if (performance.navigation.type == 1) {
 
$(document).ready(function(){
    setTimeout(function() {
        window.onscroll = function() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                $("#myBtn").css('display', 'block');
            } else {
                $("#myBtn").css('display', 'none');
            }
        };
        if (getCookie('dark-theme') === 'true'){
            $("#theme").click()
        }

        $("#theme").change(function () { 
            $("body").toggleClass("body-dark");
            $("nav").toggleClass("navbar-dark-active");
            $(".page-footer").toggleClass("navbar-dark-active");
            $("p").toggleClass("text-dark");
            $("h5").toggleClass("text-dark");
            $(".badge").toggleClass("badge-dark");
            $(".accordion-title").toggleClass("accordion-dark");
            $(".accordion-content").toggleClass("accordion-content-dark");
            $("ul").toggleClass("text-dark");
            $("li").toggleClass("text-dark");
            $("#myBtn").toggleClass("upBtn-dark");
            document.cookie = `dark-theme=${this.checked}`;
       });
       
       var topFunction = () => {
           document.body.scrollTop = 0; // For Safari
           document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
       }
       
       function getCookie(cname) {
           var name = cname + "=";
           var decodedCookie = decodeURIComponent(document.cookie);
           var ca = decodedCookie.split(';');
           for(var i = 0; i <ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') {
               c = c.substring(1);
             }
             if (c.indexOf(name) == 0) {
               return c.substring(name.length, c.length);
             }
           }
           return "";
       }
       new WOW().init();
    }, 3000);

    setTimeout(function(){
        $(".opening-1").css("opacity", "0%");    
        $(".opening-1").css("display", "none");    
        $(".opening-2").css("visibility", "visible");    
        $(".opening-2").css("opacity", "100%");    
        $(".opening-2").css("padding-top", "0px");    
    }, 1000)

    setTimeout(function(){
        $(".opening-2").css("opacity", "0%");    
        $(".opening-2").css("display", "none");    
        $(".opening-3").css("visibility", "visible");    
        $(".opening-3").css("opacity", "100%");    
        $(".opening-3").css("padding-top", "0px");       
    }, 2000)

    setTimeout(function(){
        $(".opening-3").css("opacity", "0%");
        $(".opening-3").css("display", "none");    
        $("#first-backdrop").css("bottom", "50%"); 
    }, 3000)

    setTimeout(function(){
        $("#first-backdrop").css("right", "100%"); 
    }, 3300)

    setTimeout(function(){
        $("#first-backdrop").css("display", "none"); 
    }, 3600)

    // framework animation needs 
    $(".opening-1").css("padding-top", "0px");    
});

} else{ 
    $("#first-backdrop").css("display", "none"); 
}